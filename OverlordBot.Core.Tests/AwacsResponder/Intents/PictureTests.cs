﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder.Intents
{
    /// <summary>
    /// Verify the bot responds correctly to picture requests.
    /// </summary>
    [TestClass]
    public class PictureTests
    {

        [TestMethod]
        public async Task RespondsCorrectly()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var transmission = new BasicTransmission(null, ITransmission.Intents.Picture, player, awacs);

            const string expectedResponse = "dolt 1 2, overlord, we do not support picture calls";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }
    }
}
