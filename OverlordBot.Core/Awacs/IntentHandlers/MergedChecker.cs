﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using Geo.Measure;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    public partial class WarningRadiusChecker
    {
        public const int MergedBoundary = 3; // nautical miles

        internal record MergedDetails : Reply.IReplyDetails
        {
            private readonly int _bearing;
            private readonly int _altitude;
            public readonly List<Unit> _group;
            private readonly Reply.IReplyDetails.MeasurementSystem _system;

            public MergedDetails(int bearing, int altitude, List<Unit> group, Reply.IReplyDetails.MeasurementSystem system)
            {
                _bearing = bearing;
                _altitude = altitude;
                _group = group;
                _system = system;
            }

            public string ToSpeech()
            {
                var sections = new List<string>
                {
                    "merged",
                    Reply.IReplyDetails.BearingToWords(_bearing),
                    Reply.IReplyDetails.AltitudeToWords(_altitude, _system)
                };

                sections.AddRange(Reply.IReplyDetails.UnitCountsToWords(_group));

                return string.Join(", ", sections);
            }

            public string ToText()
            {
                var distanceAcronym = _system == Reply.IReplyDetails.MeasurementSystem.Metric ? "km" : "nm";
                var altitudeAcronym = _system == Reply.IReplyDetails.MeasurementSystem.Metric ? "m" : "ft";

                int convertedAltitude;

                if (_system == Reply.IReplyDetails.MeasurementSystem.Metric)
                {
                    convertedAltitude = (int)Math.Round(_altitude / 100d, 0) * 100;
                }
                else
                {
                    convertedAltitude = (int)Math.Round(_altitude * 3.28d / 1000d, 0) * 1000;
                }

                if (convertedAltitude == 0)
                {
                    convertedAltitude = 500;
                }

                var sections = new List<string>
                {
                    $"MERGED {_bearing.ToString("D3")}",
                    $"{convertedAltitude.ToString("N0")}{altitudeAcronym}",
                };

                sections.AddRange(Reply.IReplyDetails.UnitCountsToWords(_group));

                return string.Join(" | ", sections);
            }
        }

        public static WarningCheckResult ProcessMergedContacts(Unit monitoredUnit, List<Unit> newContacts, List<Unit> reportedMergedContacts)
        {
            var contacts = FilterReportedContacts(monitoredUnit, 3, 1, newContacts, reportedMergedContacts);
            return contacts.Count == 0 ? new WarningCheckResult() : BuildMergedResponse(monitoredUnit, contacts);
        }

        private static WarningCheckResult BuildMergedResponse(Unit playerUnit, IReadOnlyCollection<Unit> contacts)
        {
            var transmitterPoint = playerUnit.Location;
            var closestContact = contacts.First();
            
            var range = (int) playerUnit.DistanceTo(closestContact, DistanceUnit.Nm);

            // Only report the closest contact & friends if the closest contact is within 3 miles
            if (range > MergedBoundary) return new WarningCheckResult();

            var bearing = (int) Geospatial.TrueToMagnetic(transmitterPoint,  playerUnit.BearingTo(closestContact));
            var altitude = (int) closestContact.Altitude;

            // ReSharper disable once PossibleUnintendedReferenceComparison
            var group = contacts.Where(contact => closestContact != contact &&
                                                  closestContact.DistanceTo(contact, DistanceUnit.Nm) < 5
            ).ToList();
            group.Add(closestContact);

            var system = BogeyDopeHandler.MetricAirframes.Contains(playerUnit.Name)
                ? Reply.IReplyDetails.MeasurementSystem.Metric
                : Reply.IReplyDetails.MeasurementSystem.Imperial;

            Logger.Debug($"Newly reported contacts:\n{LogContacts(playerUnit, group)}");
            return new WarningCheckResult
            {
                Reply = new Reply
                {
                    Details = new MergedDetails(bearing, altitude, group, system)
                },
                NewMergedContacts = group
            };
        }
    }
}
